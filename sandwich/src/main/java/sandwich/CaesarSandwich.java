package sandwich;

public class CaesarSandwich extends VegetarianSandwich{
    public CaesarSandwich(){
        this.addFilling("Caesar Dressing");;
    }
    public String getProtein(){
        return "Anchovies";
    }
    public boolean isVegetarian(){
        return false;
    }
}
