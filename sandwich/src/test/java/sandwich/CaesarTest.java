package sandwich;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class CaesarTest {
    @Test
    public void consTest(){
        CaesarSandwich c = new CaesarSandwich();
        assertEquals("Caesar Dressing", c.getFilling());
    }
}
