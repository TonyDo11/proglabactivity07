package sandwich;

public abstract class VegetarianSandwich implements ISandwich {
    private String filling;

    public VegetarianSandwich() {
        this.filling = "";
    }
    @Override 
    public String getFilling() {
        return filling.trim();
    }
    @Override
    public void addFilling(String topping) {
       String[] nonVegetarianIngredients = {"chicken", "beef", "fish", "meat", "pork"};

       for( String ingredient: nonVegetarianIngredients) {
            if(topping.contains(ingredient)) {
                throw new IllegalArgumentException("Not vegan");
            }
            
       }
       filling += topping + " ";
    }

    @Override
    public boolean isVegetarian() { /* Forces it to have a return type *if I put 'final' */
        return true;
    }
    public boolean isVegan() {
        return !filling.contains("cheese") && !filling.contains("egg");
    }
    public abstract String getProtein();

}
