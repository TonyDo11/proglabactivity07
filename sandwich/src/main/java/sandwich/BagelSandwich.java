package sandwich;

public class BagelSandwich  implements ISandwich {
    private String filling;
    public BagelSandwich(){
        this.filling ="";
    }
    public void addFilling(String topping){
        this.filling= this.filling +topping+" ";
    }
    public String getFilling(){
        return this.filling;
    }
    public boolean isVegetarian(){
        throw new UnsupportedOperationException("not supported");
    }
}
