package sandwich;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TofuTest {
    @Test
    public void consTest(){
        TofuSandwich t = new TofuSandwich();
        assertEquals("Tofu", t.getFilling());
    }
    @Test
    public void getProteinTest(){
        TofuSandwich t = new TofuSandwich();
        assertEquals("Tofu", t.getProtein());
    }
}
