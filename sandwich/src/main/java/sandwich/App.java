package sandwich;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ISandwich tofuSandwich = new TofuSandwich(); /* worked because Tofu is a subclass of vegetarian which implements ISandwich */
       /*  tofuSandwich.addFilling("chicken"); */
       /*  tofuSandwich.isVegetarian(); */ /* Also throws an error */

        VegetarianSandwich vegetarianTofuSandwich = (VegetarianSandwich)tofuSandwich; /* It does compile */


        /* VegetarianSandwich CaeserThing = new CaeserSandwich();
         * CaeserThing.isVegetarian(); -What happens?
         */

        if(tofuSandwich instanceof ISandwich) {
            System.out.println("YES"); /* It is. */
        }
        else {
            System.out.println("NO");
        }
    }
}
