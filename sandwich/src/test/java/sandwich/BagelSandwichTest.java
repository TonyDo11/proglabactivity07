package sandwich;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class BagelSandwichTest {
    private ISandwich bagelSandwich;

 

    @Test
    public void testAddFilling() {
        BagelSandwich bagelSandwich = new BagelSandwich();
        bagelSandwich.addFilling("cream cheese");
        bagelSandwich.addFilling("something food");
        assertEquals("cream cheese something food ", bagelSandwich.getFilling());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsVegetarian() {
        BagelSandwich bagelSandwich = new BagelSandwich();
        bagelSandwich.isVegetarian();
    }
    
}
